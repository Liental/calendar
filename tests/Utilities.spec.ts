import assert from 'assert'
import { Utilities } from '../src/classes/Utilities'

/** Test Utilities class */
describe('Utilities', () => {
  /** Test shift date method using the 23rd of May 2019 as an example */
  describe('#shiftDate', () => {
    const date = new Date('05-23-2019')

    it ('shifting one day back should return 22.05.2019', () => {
      const expected = new Date('05-22-2019').getTime()
      const actual = Utilities.shiftDate(date, -1).getTime()

      assert.equal(actual, expected)
    })

    it ('shifting one day forwards should return 24.05.2019', () => {
      const expected = new Date('05-24-2019').getTime()
      const actual = Utilities.shiftDate(date, 1).getTime()

      assert.equal(actual, expected)
    })
  })
  
  /** Test first day of the month method using the 23rd of May 2019 as an example */
  describe('#getFirstDay', () => {
    const date = new Date('05-23-2019')

    it ('getting first day of the month should return 01.05.2019', () => {
      const expected = new Date('05-01-2019').getTime()
      const actual = Utilities.getFirstDay(date).getTime()

      assert.equal(actual, expected)
    })
  })

  /** Test get month mothod using the 23rd of May 2019  */
  describe('#getMonth', () => {
    const date = new Date('05-23-2019')
    
    it ('getting whole month should return array of 6 weeks and 42 days of May 2019', () => {
      const actual = Utilities.getMonth(date)
      const expected = [[{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!0,"dayNumber":29,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!0,"dayNumber":30,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":1,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":2,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":3,"availableHours":[]},{"isWorkFree":!0,"isToday":!1,"isMiscellaneous":!1,"dayNumber":4,"availableHours":[]},{"isWorkFree":!0,"isToday":!1,"isMiscellaneous":!1,"dayNumber":5,"availableHours":[]}],[{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":6,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":7,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":8,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":9,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":10,"availableHours":[]},{"isWorkFree":!0,"isToday":!1,"isMiscellaneous":!1,"dayNumber":11,"availableHours":[]},{"isWorkFree":!0,"isToday":!1,"isMiscellaneous":!1,"dayNumber":12,"availableHours":[]}],[{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":13,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":14,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":15,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":16,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":17,"availableHours":[]},{"isWorkFree":!0,"isToday":!1,"isMiscellaneous":!1,"dayNumber":18,"availableHours":[]},{"isWorkFree":!0,"isToday":!1,"isMiscellaneous":!1,"dayNumber":19,"availableHours":[]}],[{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":20,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":21,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":22,"availableHours":[]},{"isWorkFree":!1,"isToday":!0,"isMiscellaneous":!1,"dayNumber":23,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":24,"availableHours":[]},{"isWorkFree":!0,"isToday":!1,"isMiscellaneous":!1,"dayNumber":25,"availableHours":[]},{"isWorkFree":!0,"isToday":!1,"isMiscellaneous":!1,"dayNumber":26,"availableHours":[]}],[{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":27,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":28,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":29,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":30,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!1,"dayNumber":31,"availableHours":[]},{"isWorkFree":!0,"isToday":!1,"isMiscellaneous":!0,"dayNumber":1,"availableHours":[]},{"isWorkFree":!0,"isToday":!1,"isMiscellaneous":!0,"dayNumber":2,"availableHours":[]}],[{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!0,"dayNumber":3,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!0,"dayNumber":4,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!0,"dayNumber":5,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!0,"dayNumber":6,"availableHours":[]},{"isWorkFree":!1,"isToday":!1,"isMiscellaneous":!0,"dayNumber":7,"availableHours":[]},{"isWorkFree":!0,"isToday":!1,"isMiscellaneous":!0,"dayNumber":8,"availableHours":[]},{"isWorkFree":!0,"isToday":!1,"isMiscellaneous":!0,"dayNumber":9,"availableHours":[]}]]
      
      const actualJSON = JSON.stringify(actual)
      const expectedJSON = JSON.stringify(expected)

      assert.equal(actualJSON, expectedJSON)
    })
  })
})
