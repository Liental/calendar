module.exports = {
    entry: './src/index.tsx',
    
    resolve: {
        extensions: [ '.ts', '.tsx', '.js', '.json' ]
    },

    output: {
        filename: 'index.js',
        path: __dirname + '/dist'
    },

    module: {
        rules: [ 
            { test: /\.tsx?$/, loader: 'ts-loader' },
            { test: /\.css$/, use: [ 'style-loader', 'css-loader' ] }
        ]
    }
}