import * as React from 'react'

import './Week.component.css'
import { Day } from '../classes/Day'
import { DayComponent } from './Day.component'

interface WeekProps { days: Day[] }

/** 
 * WeekComponent contains 7 Day objects props representing a week. \
 * If more than 7 days is provided, take first 7 in the array.
 */
export class WeekComponent extends React.Component<WeekProps> {
  public render () {
    const daysComponents = this.props.days.map((day: Day) => <DayComponent day={day} />)

    return (
      <div className='week-container'>
        {daysComponents.slice(0, 7)}
      </div>
    )
  }
}
