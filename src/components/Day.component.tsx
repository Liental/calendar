import * as React from 'react'

import './Day.component.css'
import { Day } from '../classes/Day'

interface DayProps { day: Day }

/** DayComponent contains a Day prop and renders it */
export class DayComponent extends React.Component<DayProps> {
  public render () {
    const day = this.props.day
    
    const containerClass = [
      'day-container',
      (day.isToday ? 'today' : ''),
      (day.isWorkFree ? 'work-free' : ''),
      (day.isMiscellaneous ? 'miscellaneous' : '')
    ].join(' ')

    return (
      <div className={containerClass}>
        <span className='day-number'>{day.dayNumber}</span>
      </div>
    )
  }
}
