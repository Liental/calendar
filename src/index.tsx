import * as React from 'react'
import * as ReactDOM from 'react-dom'

import { Day } from './classes/Day'
import { Utilities } from './classes/Utilities'
import { WeekComponent } from './components/Week.component'

const root = document.getElementById('root')

main()
function main () {
  const currentDate = new Date()
  const currentMonth = Utilities.getMonth(currentDate)
  const weekComponents = currentMonth.map((week: Day[]) => <WeekComponent days={week} />)

  ReactDOM.render((
    <div>
      {weekComponents}
    </div>
  ), root)
}
