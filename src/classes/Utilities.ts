import { Day } from './Day'

/** A class containing miscellaneous static methods that help in development */
export class Utilities {
  /**
   * Shift a date by the number of provided days
   * 
   * @param date - a date to shift the day
   * @param days - number of days to shift the date
   * @returns the provided date shiftied by provided number of days 
   */
  public static shiftDate (date: Date, days: number): Date {
    const time = date.getTime()
    const timeOffset = days * (1000 * 60 * 60 * 24)

    return new Date(time + timeOffset)
  }

  /**
   * Shift provided date to the first day of its month
   * 
   * @param date - a date to calculate the first day from
   * @returns the first day of a month
   */
  public static getFirstDay (date: Date): Date {
    const currentDay = date.getDate()
    const firstDay = this.shiftDate(date, -currentDay + 1)

    return firstDay
  }

  /** 
   * Get 5 arrays of Day objects containing 5 weeks of provided months and fills gaps with 
   * the start and/or the end the next and previous months
   *
   * @param date - a date to get current month from 
   * @returns an array containing 5 arrays of Day objects
   */
  public static getMonth (date: Date): Day[][] {
    const daysInMonth: Day[][] = []

    const firstMonthDay = this.getFirstDay(date)
    const firstWeekDay = firstMonthDay.getDay()

    // If the first day of the month is not a monday, take the rest of the week to fill it
    const firstDay = (firstWeekDay === 1 ? firstMonthDay : this.shiftDate(firstMonthDay, -firstWeekDay + 1))
    
    for (let i = 0; i < 42; i++) {
      const dayToPush = new Day()
      const dateToUse = this.shiftDate(firstDay, i)

      const dayOfWeek = dateToUse.getDay()
      const dayOfMonth = dateToUse.getDate() 
      
      dayToPush.dayNumber = dayOfMonth
      dayToPush.isWorkFree = (dayOfWeek === 6 || dayOfWeek === 0)
      dayToPush.isMiscellaneous = date.getMonth() !== dateToUse.getMonth() 
      dayToPush.isToday = (date.getDate() === dayOfMonth && date.getMonth() === dateToUse.getMonth())
      
      if (dayOfWeek === 1)
        daysInMonth.push([ ])

      daysInMonth[daysInMonth.length - 1].push(dayToPush)
    }

    return daysInMonth
  }
}
