/** Day class representing a day of work in a month */
export class Day {
  /** Define if the day is available to book a visit in */
  public isWorkFree: boolean = false
  
  /** Define if the day matches current date */
  public isToday: boolean = false

  /** Define if the day is from another category (i.e. to fill the gaps for example) */
  public isMiscellaneous: boolean = false

  /** Number of the day in a month */
  public dayNumber: number = 0

  /** Hours available to book a visit in */
  public availableHours: any[] = []
}
